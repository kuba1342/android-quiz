package com.example.kuba.quiz;

public class Question {
    int mTextResId;
    boolean mAnswerTrue;
    boolean mAnswered;

    Question(int textResId, boolean answerTrue) {
        mTextResId = textResId;
        mAnswerTrue = answerTrue;
        mAnswered = false;
    }

    int getTextResId() {
        return mTextResId;
    }

    void setTextResId(int textResId) {
        mTextResId = textResId;
    }

    boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }

    boolean isAnswered() {
        return mAnswered;
    }

    void setAnswered(boolean answered) {
        mAnswered = answered;
    }
}
