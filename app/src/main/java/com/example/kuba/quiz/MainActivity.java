package com.example.kuba.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button mTrueButton;
    Button mFalseButton;
    Button mNextButton;
    ImageButton mPreviousButton;
    private TextView mQuestionTextView;
    int mCurrentIndex = 0;
    int mUserAnsweres = 0;
    int mUserScore = 0;

    Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_stolica_polski, true),
            new Question(R.string.question_stolica_dolnego_slaska, false),
            new Question(R.string.question_sniezka, true),
            new Question(R.string.question_wisla, true)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFalseButton = (Button) findViewById(R.id.buttonFalse);
        mTrueButton = (Button) findViewById(R.id.buttonTrue);
        mPreviousButton = (ImageButton) findViewById(R.id.buttonPrevious);
        mQuestionTextView = (TextView) findViewById(R.id.textView);
        mQuestionTextView.setText(mQuestionBank[mCurrentIndex].getTextResId());

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateQuestion();
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(false);
            }
        });

        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(true);
            }
        });

        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previousQuestion();
            }
        });

//        mNextButton.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                updateQuestion();
////            }
////        });
    }

    void updateQuestion() {
        if (mCurrentIndex != 3) {
            mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
            int questionIndex = mQuestionBank[mCurrentIndex].getTextResId();
            mQuestionTextView.setText(questionIndex);
        }
    }

    void displayScore() {
        if (mUserAnsweres == mQuestionBank.length) {
            String scoreToDisplay = "Twoja liczba punktów: " + mUserScore;
            Toast scoreToast = Toast.makeText(this, scoreToDisplay, Toast.LENGTH_LONG);
            scoreToast.setGravity(Gravity.BOTTOM, 0, 150);
            scoreToast.show();
        }
    }

    void previousQuestion() {
        if (mCurrentIndex != 0) {
            mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
            int questionIndex = mQuestionBank[mCurrentIndex].getTextResId();
            mQuestionTextView.setText(questionIndex);
        }
    }

    void checkAnswer(boolean userPressedTrue) {
        int messageToDisplay;
        if (!mQuestionBank[mCurrentIndex].isAnswered()) {
            if (mQuestionBank[mCurrentIndex].isAnswerTrue() == userPressedTrue) {
                messageToDisplay = R.string.true_toast;
                mUserScore += 1;
            } else {
                messageToDisplay = R.string.false_toast;
            }
            mQuestionBank[mCurrentIndex].setAnswered(true);

            mUserAnsweres += 1;
        } else {
            messageToDisplay = R.string.answered_toast;
        }
        Toast toastDisplay = Toast.makeText(this, messageToDisplay, Toast.LENGTH_SHORT);
        toastDisplay.setGravity(Gravity.TOP, 0, 150);
        toastDisplay.show();

        displayScore();
    }
}
